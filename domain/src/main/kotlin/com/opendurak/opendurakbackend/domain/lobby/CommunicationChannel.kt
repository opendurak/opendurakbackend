package com.opendurak.opendurakbackend.domain.lobby

import com.opendurak.opendurakbackend.domain.lobby.model.GameState
import com.opendurak.opendurakbackend.domain.lobby.model.UserId

public interface CommunicationChannel {

    public suspend fun send(content: GameState?)

    public suspend fun send(content: Collection<String>)

    public suspend fun send(content: UserId)

    public suspend fun close()
}