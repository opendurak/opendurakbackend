package com.opendurak.opendurakbackend.domain.game.model

@JvmInline
internal value class Battlefield(val map: MutableMap<Card, Card?> = mutableMapOf()) {

    fun getAttackingCards() = map.keys.toList()

    fun getDefendingCard(attackingCard: Card) = map[attackingCard]

    fun addAttackingCard(attackingCard: Card) {
        map[attackingCard] = null
    }

    @Throws(IllegalArgumentException::class)
    fun addDefendingCard(defendingCard: Card, attackingCard: Card) {
        if (map.containsKey(attackingCard)) map[attackingCard] = defendingCard
        else throw IllegalArgumentException("Card is not attacking")
    }

    fun areAllCardsDefended() = !map.containsValue(null)

    fun getNumberOfCardsNotDefended() = map.count { it.value == null }

    fun getAllCards() = map.flatMap { listOfNotNull(it.key, it.value) }

    fun getNumberOfStacks() = map.count()

    fun clearBattlefield() {
        map.clear()
    }
}