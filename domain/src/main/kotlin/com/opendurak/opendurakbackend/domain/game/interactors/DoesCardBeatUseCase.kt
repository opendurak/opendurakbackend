package com.opendurak.opendurakbackend.domain.game.interactors

import com.opendurak.opendurakbackend.domain.game.model.Card

internal class DoesCardBeatUseCase {

    fun execute(card: Card, otherCard: Card, trumpCard: Card): Boolean {
        val isCardTrump = card.suit == trumpCard.suit
        val isOtherCardTrump = otherCard.suit == trumpCard.suit
        val isOtherCardWeightStronger = card.value.weight > otherCard.value.weight

        return if (isCardTrump && isOtherCardTrump) isOtherCardWeightStronger
        else if (!isCardTrump && !isOtherCardTrump) {
            if (card.suit == otherCard.suit) isOtherCardWeightStronger
            else false
        } else /* one is trump */ isCardTrump
    }
}