package com.opendurak.opendurakbackend.domain.game.interactors

import com.opendurak.opendurakbackend.domain.game.model.Battlefield
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.game.model.Player
import com.opendurak.opendurakbackend.domain.lobby.model.UserId

internal class CreateGameUseCase(
    private val fillHandTo6CardsUseCase: FillHandTo6CardsUseCase,
) {

    @Throws(IllegalArgumentException::class)
    fun execute(
        playerIds: List<UserId>,
    ): Game {
        if (playerIds.size < 2 || playerIds.size > 5) throw IllegalArgumentException("Must be between 2 and 5 players")

        val stack = createDeck().apply { shuffle() }
        val players = playerIds
            .shuffled()
            .mapIndexed { index: Int, id: UserId -> Player(id, index, mutableListOf()) }
            .toMutableList()
        players.forEach { fillHandTo6CardsUseCase.execute(it, stack) }
        val trump = stack.last()

        val attacker = players[0]
        val defender = players[1]
        val helper = players.getOrNull(2)

        return Game(
            players, stack, trump, Battlefield(), attacker, defender, helper,
            attackerGaveUp = false, defenderGaveUp = false, helperGaveUp = false, gameFinished = false
        )
    }

    private fun createDeck(): MutableList<Card> {
        val cards = mutableListOf<Card>()
        Card.Suit.values().forEach { suit ->
            Card.Value.values().forEach { value ->
                cards.add(Card(suit, value))
            }
        }
        return cards
    }

}