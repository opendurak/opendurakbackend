package com.opendurak.opendurakbackend.domain.lobby.interactor

import com.opendurak.opendurakbackend.domain.lobby.model.UserHash
import com.opendurak.opendurakbackend.domain.lobby.model.UserId
import java.security.MessageDigest

internal class HashUserIdUseCase(
    private val md: MessageDigest,
) {
    fun hash(userId: UserId): UserHash {
        val digest = md.digest(userId.toByteArray())
        return digest.fold("") { str, it ->
            str + "%02x".format(it)
        }
    }
}