package com.opendurak.opendurakbackend.domain.game.interactors

import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Player

internal class FillHandTo6CardsUseCase {
    fun execute(player: Player, stack: MutableList<Card>) {
        while (player.cards.size < 6) {
            val card = stack.removeFirstOrNull()
            if (card != null) player.cards.add(card)
            else return // no more cards
        }
    }
}