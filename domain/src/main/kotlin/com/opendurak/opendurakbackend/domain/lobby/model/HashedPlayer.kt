package com.opendurak.opendurakbackend.domain.lobby.model

import com.opendurak.opendurakbackend.domain.game.model.Card

public data class HashedPlayer internal constructor(
    val id: UserHash,
    val cards: List<Card>,
)