package com.opendurak.opendurakbackend.domain.game

import com.opendurak.opendurakbackend.domain.game.interactors.*
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.game.model.Player
import com.opendurak.opendurakbackend.domain.lobby.Lobby
import com.opendurak.opendurakbackend.domain.lobby.LobbyManager
import com.opendurak.opendurakbackend.domain.lobby.interactor.SendPersonalGameStateUseCase
import com.opendurak.opendurakbackend.domain.lobby.model.LobbyId
import com.opendurak.opendurakbackend.domain.lobby.model.UserId
import kotlinx.coroutines.sync.withLock

internal class GameApiFacadeImpl(
    private val lobbyManager: LobbyManager,
    private val createGameUseCase: CreateGameUseCase,
    private val attackOnBattlefieldUseCase: AttackOnBattlefieldUseCase,
    private val defendOnBattlefieldUseCase: DefendOnBattlefieldUseCase,
    private val giveUpUseCase: GiveUpUseCase,
    private val helpOnBattlefieldUseCase: HelpOnBattlefieldUseCase,
    private val sendPersonalGameStateUseCaseFactory: (Lobby) -> SendPersonalGameStateUseCase,
    private val progressRoundUseCase: ProgressRoundUseCase,
) : GameApiFacade {

    @Throws(IllegalArgumentException::class)
    override suspend fun start(lobbyId: LobbyId, userId: UserId) {
        val lobby = lobbyManager.findLobby(lobbyId)
            ?: throw IllegalArgumentException("Lobby not found")
        requireUserIsInLobby(userId, lobby)

        lobby.gameMutex.withLock {
            val somePlayers = lobby.users.keys.shuffled().take(5)
            lobby.game = createGameUseCase.execute(somePlayers)
            broadcastGameState(lobby)
        }
    }

    override suspend fun attack(card: Card, lobbyId: LobbyId, userId: UserId) {
        runGameLogic(userId, lobbyId) { game, lobby, player ->
            attackOnBattlefieldUseCase.execute(card, player, game)
            progressRoundUseCase.execute(game)
            broadcastGameState(lobby)
        }
    }

    override suspend fun help(card: Card, lobbyId: LobbyId, userId: UserId) {
        runGameLogic(userId, lobbyId) { game, lobby, player ->
            helpOnBattlefieldUseCase.execute(card, player, game)
            progressRoundUseCase.execute(game)
            broadcastGameState(lobby)
        }
    }

    override suspend fun defend(defenderCard: Card, attackerCard: Card, lobbyId: LobbyId, userId: UserId) {
        runGameLogic(userId, lobbyId) { game, lobby, player ->
            defendOnBattlefieldUseCase.execute(defenderCard, attackerCard, player, game)
            progressRoundUseCase.execute(game)
            broadcastGameState(lobby)
        }
    }

    override suspend fun giveUp(lobbyId: LobbyId, userId: UserId) {
        runGameLogic(userId, lobbyId) { game, lobby, player ->
            giveUpUseCase.execute(player, game)
            progressRoundUseCase.execute(game)
            broadcastGameState(lobby)
        }
    }

    @Throws(IllegalArgumentException::class)
    private suspend fun runGameLogic(
        userId: UserId,
        lobbyId: LobbyId,
        block: suspend (Game, Lobby, Player) -> Unit,
    ) {
        val lobby = getLobby(lobbyId)
        requireUserIsInLobby(userId, lobby)

        lobby.gameMutex.withLock {
            val game = lobby.game
            if (game != null && !game.gameFinished) {
                block(game, lobby, getPlayer(userId, game))
            } else {
                throw IllegalArgumentException("Game not found or not running")
            }
        }
    }

    @Throws(IllegalArgumentException::class)
    private fun getPlayer(playerId: UserId, game: Game) = game.players.find { it.id == playerId }!!

    override fun getLobby(lobbyId: LobbyId) = lobbyManager.findLobby(lobbyId)!!

    override fun requireUserIsInLobby(userId: UserId, lobby: Lobby) {
        require(lobby.users.containsKey(userId))
    }

    private suspend fun broadcastGameState(lobby: Lobby) {
        sendPersonalGameStateUseCaseFactory.invoke(lobby).broadcastToLobby()
    }
}