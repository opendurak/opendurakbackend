package com.opendurak.opendurakbackend.domain.game.interactors

import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.game.model.Player

internal class GiveUpUseCase {

    @Throws(IllegalArgumentException::class, IllegalStateException::class)
    fun execute(player: Player, game: Game) {
        when (player) {
            game.attacker ->
                if (offendersCanGiveUp(game)) game.attackerGaveUp = true
                else throw IllegalStateException("Attacker cannot give up now")
            game.defender ->
                if (defenderCanGiveUp(game)) game.defenderGaveUp = true
                else throw IllegalStateException("Defender cannot give up now")
            game.helper ->
                if (offendersCanGiveUp(game)) game.helperGaveUp = true
                else throw IllegalStateException("Helper cannot give up now")
            else -> throw IllegalArgumentException("Player cannot give up")
        }
    }

    private fun offendersCanGiveUp(game: Game): Boolean {
        return game.battlefield.getNumberOfStacks() != 0 && game.battlefield.areAllCardsDefended()
    }

    private fun defenderCanGiveUp(game: Game): Boolean {
        return game.battlefield.getNumberOfStacks() != 0 && !game.battlefield.areAllCardsDefended()
    }
}