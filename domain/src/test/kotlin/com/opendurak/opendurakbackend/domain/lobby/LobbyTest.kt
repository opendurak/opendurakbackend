package com.opendurak.opendurakbackend.domain.lobby

import com.google.common.truth.Truth.assertThat
import com.opendurak.opendurakbackend.domain.lobby.interactor.SendPersonalGameStateUseCase
import com.opendurak.opendurakbackend.domain.lobby.model.GameState
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Test

class LobbyTest {
    private val personalGameStateUseCase = mockk<SendPersonalGameStateUseCase>(relaxUnitFun = true)
    private val sendPersonalGameStateUseCaseFactory = { _: Lobby -> personalGameStateUseCase }
    private var currentUuid = 69
    private val uuidFactory = { (currentUuid++).toString() }
    private val lobby = Lobby(sendPersonalGameStateUseCaseFactory, uuidFactory)
        .apply { lobbyId = "42" }

    @Test
    fun `when initialized game should beNull`() {
        assertThat(lobby.game).isNull()
    }

    @Test
    fun `when initialized users should beEmpty`() {
        assertThat(lobby.users).hasSize(0)
    }

    @Test
    fun `when userAdded users should returnUser`() {
        runBlocking { lobby.joinLobby("Peter") }

        val users = lobby.users.toList()
        assertThat(users).hasSize(1)
        val actualUser = users[0]

        assertThat(actualUser.first).isEqualTo("69")
        assertThat(actualUser.second).isEqualTo(Lobby.User("Peter", null, null))
    }

    @Test
    fun `joinLobby should returnUserId`() {
        val userId = runBlocking {
            lobby.joinLobby("Peter")
        }
        assertThat(userId).isEqualTo("69")
    }

    @Test
    fun `joinLobby should notifyAllUsersAboutLobbyUsers`() {
        val lobbyChannel = createMockCommunicationChannel()
        runBlocking { lobby.joinLobby("Peter") }
        runBlocking { lobby.subscribeLobby("69", lobbyChannel) }
        runBlocking { lobby.joinLobby("Marc") }
        coVerify(exactly = 1) { lobbyChannel.send(listOf("Peter", "Marc")) }
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when userNotInLobby subscribeLobby should throwException`() {
        val lobbyChannel = createMockCommunicationChannel()
        runBlocking { lobby.subscribeLobby("69", lobbyChannel) }
    }

    @Test
    fun `subscribeLobby should replaceChannel`() {
        val lobbyChannel = createMockCommunicationChannel()
        val lobby2Channel = createMockCommunicationChannel()
        runBlocking { lobby.joinLobby("Hans") }
        runBlocking { lobby.subscribeLobby("69", lobbyChannel) }
        runBlocking { lobby.subscribeLobby("69", lobby2Channel) }
        assertThat(lobby.users["69"]!!.lobbyCommunicationChannel).isEqualTo(lobby2Channel)
    }

    @Test
    fun `subscribeLobby should sendLobbyToNewChannel`() {
        val lobbyChannel = createMockCommunicationChannel()
        val lobby2Channel = createMockCommunicationChannel()
        runBlocking { lobby.joinLobby("Hans") }
        runBlocking { lobby.subscribeLobby("69", lobbyChannel) }
        runBlocking { lobby.subscribeLobby("69", lobby2Channel) }
        coVerify { lobby2Channel.send(listOf("Hans")) }
    }

    @Test
    fun `unsubscribeToLobby should unassignLobbyChannel`() {
        val lobbyChannel = createMockCommunicationChannel()
        runBlocking { lobby.joinLobby("Hans") }
        runBlocking { lobby.subscribeLobby("69", lobbyChannel) }
        runBlocking { lobby.unsubscribeToLobby("69", lobbyChannel) }
        assertThat(lobby.users["69"]!!.lobbyCommunicationChannel).isNull()
    }

    @Test
    fun `when thereIsADifferentChannel unsubscribeToLobby should notDoAnything`() {
        val lobbyChannel = createMockCommunicationChannel()
        val lobby2Channel = createMockCommunicationChannel()
        runBlocking { lobby.joinLobby("Marckus") }
        runBlocking { lobby.subscribeLobby("69", lobbyChannel) }
        runBlocking { lobby.subscribeLobby("69", lobby2Channel) }
        runBlocking { lobby.unsubscribeToLobby("69", lobbyChannel) }
        assertThat(lobby.users["69"]!!.lobbyCommunicationChannel).isEqualTo(lobby2Channel)
    }

    @Test
    fun `when userDoesntExist unsubscribeToLobby should notDoAnything`() {
        val lobbyChannel = createMockCommunicationChannel()
        runBlocking { lobby.unsubscribeToLobby("69", lobbyChannel) }
        assertThat(lobby.users["69"]).isNull()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when userDoesntExist subscribeToGameState should throwException`() {
        val gameStateChannel = createMockCommunicationChannel()
        runBlocking { lobby.subscribeToGameState("69", gameStateChannel) }
    }

    @Test
    fun `when gameNotStarted subscribeToGameState should notSendAnything`() {
        runBlocking { lobby.joinLobby("Marckus") }
        val gameStateChannel = createMockCommunicationChannel()
        runBlocking { lobby.subscribeToGameState("69", gameStateChannel) }
        coVerify(exactly = 0) { gameStateChannel.send(any<GameState>()) }
    }

    @Test
    fun `when gameStarted subscribeToGameState should sendGameState`() {
        runBlocking { lobby.joinLobby("Marckus") }
        lobby.game = mockk()
        val gameStateChannel = createMockCommunicationChannel()
        runBlocking { lobby.subscribeToGameState("69", gameStateChannel) }
        coVerify(exactly = 1) { personalGameStateUseCase.sendToUser("69") }
    }

    @Test
    fun `when gameStarted unsubscribeToGameState should unassignGameStateChannel`() {
        runBlocking { lobby.joinLobby("Marckus") }
        val gameStateChannel = createMockCommunicationChannel()
        runBlocking { lobby.subscribeToGameState("69", gameStateChannel) }
        runBlocking { lobby.unsubscribeToGameState("69", gameStateChannel) }
        assertThat(lobby.users["69"]!!.gameStateCommunicationChannel).isNull()
    }

    private fun createMockCommunicationChannel() = mockk<CommunicationChannel>(relaxUnitFun = true)
}