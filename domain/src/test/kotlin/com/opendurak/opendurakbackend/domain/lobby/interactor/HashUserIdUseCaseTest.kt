package com.opendurak.opendurakbackend.domain.lobby.interactor

import com.google.common.truth.Truth.assertThat
import org.junit.Test
import java.security.MessageDigest
import java.util.*

class HashUserIdUseCaseTest {
    @Test
    fun `hash should returnAnUnemptyString`() {
        val hashUserIdUseCase = HashUserIdUseCase(MessageDigest.getInstance("MD5"))
        val actual = hashUserIdUseCase.hash(UUID.randomUUID().toString())
        assertThat(actual).isNotEmpty()
    }
}