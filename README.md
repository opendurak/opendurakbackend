# OpenDurakBackend

## Description

This is the backend for OpenDurak.

## Installation

- ./gradlew run

## Usage

The entrypoint are lobbies. A user should join a lobby. A lobby can have one game.

- List existing lobbies with GET to /lobby. The result is an array of lobby ids (String).
- Create a new lobby with PUT to /lobby. The result is a lobby id (String).
- You can join a lobby with opening a POST to /lobby/{lobbyId}/join. There must be a parameter "username". The server will
  respond with a userId (String).
- You can get lobby updates with a WEBSOCKET to /lobby/{lobbyId}/listenLobby. There must be a parameter "userid". The
  server will respond with an array of usernames initially and every time somebody leaves or joins the lobby.
- You can open a second WEBSOCKET to /lobby/{lobbyId}/game/listenGameState. There must be a parameter "userid". When there
  was no game yet, null is returned. When there is a game running, its game state is returned where gameFinished is
  false. When the last game is finished, the last game state is returned where gameFinished is true. Every time there is
  a change in the game state the server will return it.
- You can start a new game with a POST to /lobby/{lobbyId}/game/start. There must be a parameter "userid". There must be at
  least 2 players in the lobby and if there are more than 5 players, random 5 players will be chosen.
- The attacker of a round can attack with a POST to /lobby/{lobbyId}/game/attack. There must be a parameter "userid". The
  card to attack must be in the body.
- The helper of a round can help with a POST to /lobby/{lobbyId}/game/help. There must be a parameter "userid". The card to
  help must be in the body.
- The defender of a round can defend with a POST to /lobby/{lobbyId}/game/defend. There must be a parameter "userid". In
  the body there must be a DefendRequest object that has the card that should defend and card that should be defended.
- The attacker, helper and defender can give up their round with a POST to /lobby/{lobbyId}/game/giveUp. There must be a
  parameter "userid".

For all requests there are validations and in case there are issues the server responds with the appropiate
HttpStatusCodes.
