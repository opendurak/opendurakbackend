package com.opendurak.opendurakbackend.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.opendurak.opendurakbackend.domain.lobby.LobbyManager
import io.ktor.http.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

class LobbiesApi(
    private val lobbyApi: LobbyApi,
    private val lobbyManager: LobbyManager,
    private val objectMapper: ObjectMapper,
) {
    fun route(route: Route) = with(route) {
        route("{lobbyId}") { lobbyApi.route(this) }

        get {
            context.respond(HttpStatusCode.OK, lobbyManager.getAllLobbies().map { it.lobbyId })
        }

        put {
            val lobbyId = lobbyManager.createNewLobby().lobbyId
            val lobbyIdJson = objectMapper.writeValueAsString(lobbyId)
            context.respondText(lobbyIdJson, ContentType.Application.Json, HttpStatusCode.OK)
        }
    }
}
