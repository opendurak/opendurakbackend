package com.opendurak.opendurakbackend

import io.ktor.server.application.*

fun ApplicationCall.getLobbyId() = parameters["lobbyId"]!!

fun ApplicationCall.getUserId() = parameters["userId"]

fun ApplicationCall.getUserName() = parameters["username"]