package com.opendurak.opendurakbackend.mapper

import com.opendurak.opendurakapi.Card as ApiCard
import com.opendurak.opendurakapi.Card.Suit as ApiSuit
import com.opendurak.opendurakapi.Card.Value as ApiValue
import com.opendurak.opendurakbackend.domain.game.model.Card as DomainCard
import com.opendurak.opendurakbackend.domain.game.model.Card.Suit as DomainSuit
import com.opendurak.opendurakbackend.domain.game.model.Card.Value as DomainValue

object CardMapper {
    fun ApiCard.mapToDomain(): DomainCard {
        return DomainCard(
            when (suit) {
                ApiSuit.HEART -> DomainSuit.HEART
                ApiSuit.DIAMOND -> DomainSuit.DIAMOND
                ApiSuit.CLUB -> DomainSuit.CLUB
                ApiSuit.SPADE -> DomainSuit.SPADE
            },
            when (value) {
                ApiValue.SIX -> DomainValue.SIX
                ApiValue.SEVEN -> DomainValue.SEVEN
                ApiValue.EIGHT -> DomainValue.EIGHT
                ApiValue.NINE -> DomainValue.NINE
                ApiValue.TEN -> DomainValue.TEN
                ApiValue.JACK -> DomainValue.JACK
                ApiValue.QUEEN -> DomainValue.QUEEN
                ApiValue.KING -> DomainValue.KING
                ApiValue.ACE -> DomainValue.ACE
            }
        )
    }
}