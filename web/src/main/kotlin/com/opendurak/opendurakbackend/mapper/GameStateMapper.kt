package com.opendurak.opendurakbackend.mapper

import com.opendurak.opendurakapi.Player
import com.opendurak.opendurakapi.PlayerWithOpenCards
import com.opendurak.opendurakapi.Card as ApiCard
import com.opendurak.opendurakapi.Card.Suit as ApiSuit
import com.opendurak.opendurakapi.Card.Value as ApiValue
import com.opendurak.opendurakapi.GameState as ApiGameState
import com.opendurak.opendurakbackend.domain.game.model.Card as DomainCard
import com.opendurak.opendurakbackend.domain.game.model.Card.Suit as DomainSuit
import com.opendurak.opendurakbackend.domain.game.model.Card.Value as DomainValue
import com.opendurak.opendurakbackend.domain.lobby.model.GameState as DomainGameState


object GameStateMapper {
    fun DomainGameState.mapToApi(): ApiGameState {
        return ApiGameState(
            selfPlayer?.let { PlayerWithOpenCards(it.id, it.cards.map { it.mapToApi() }) },
            players.map { Player(it.hash, it.username, it.cardCount) },
            stackCount,
            trump.mapToApi(),
            battlefield.map { Pair(it.key.mapToApi(), it.value?.mapToApi()) },
            attackerHash, defenderHash, helperHash, attackGaveUp, defenderGaveUp, helperGaveUp,
            gameFinished
        )
    }

    private fun DomainCard.mapToApi(): ApiCard {
        return ApiCard(
            when (suit) {
                DomainSuit.HEART -> ApiSuit.HEART
                DomainSuit.DIAMOND -> ApiSuit.DIAMOND
                DomainSuit.CLUB -> ApiSuit.CLUB
                DomainSuit.SPADE -> ApiSuit.SPADE
            },
            when (value) {
                DomainValue.SIX -> ApiValue.SIX
                DomainValue.SEVEN -> ApiValue.SEVEN
                DomainValue.EIGHT -> ApiValue.EIGHT
                DomainValue.NINE -> ApiValue.NINE
                DomainValue.TEN -> ApiValue.TEN
                DomainValue.JACK -> ApiValue.JACK
                DomainValue.QUEEN -> ApiValue.QUEEN
                DomainValue.KING -> ApiValue.KING
                DomainValue.ACE -> ApiValue.ACE
            }
        )
    }
}